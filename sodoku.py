#-------------------------------------
#Leandro Atero Catalan
#Ingeniería Civil Electrónica
#Universidad Austral de Chile
#Software para ingeniería Electrónica
#-------------------------------------

#se escribe la tabla del sodoku imcompleta

tabla_sodoku = [
    [5,3,0,0,7,0,0,0,0],[6,0,0,1,9,5,0,0,0],[0,9,8,0,0,0,0,6,0],
    [8,0,0,0,6,0,0,0,3],[4,0,0,8,0,3,0,0,1],[7,0,0,0,2,0,0,0,6],
    [0,6,0,0,0,0,2,8,0],[0,0,0,4,1,9,0,0,5],[0,0,0,0,8,0,0,7,9]]

 #creacion de una funcion que resuelve el sodoku
def resolver(sodoku):
    find = find_empty(sodoku)
    if not find:
        return True
    else:
        fila, columna = find

    for i in range(1,10):
        if valid(sodoku, i, (fila, columna)):
            sodoku[fila][columna] = i

            if resolver(sodoku):
                return True

            sodoku[fila][columna] = 0

    return False
    
    #verificamos si la posicion actual es valida
def valid(sodoku, numero, posicion):

   # En este paso verificamos las filas
    for i in range(len(sodoku[0])):
        if sodoku[posicion[0]][i] == numero and posicion[1] != i:
            return False

   # En este paso verificamos las columnas
    for i in range(len(sodoku)):
        if sodoku[i][posicion[1]] == numero and posicion[0] != i:
            return False

    #Paso para verificar las casillas
    x = posicion[1] // 3
    y = posicion[0] // 3

    for i in range(y*3, y*3 + 3):
        for j in range(x * 3, x*3 + 3):
            if sodoku[i][j] == numero and (i,j) != posicion:
                return False

    return True

def print_board(sodoku):
    for i in range(len(sodoku)):
        if i % 3 == 0 and i != 0:
            print("______________________")

        for j in range(len(sodoku[0])):
            if j % 3 == 0 and j != 0:
                print("| ", end="")

            if j == 8:
                print(sodoku[i][j])
            else:
                print(str(sodoku[i][j]) + " ", end="")

 # i "filas", "j"columnas, recorre el tablero buscando un espacio vacio
def find_empty(sodoku):
    for i in range(len(sodoku)):
        for j in range(len(sodoku[0])):
            if sodoku[i][j] == 0:
                return (i, j) 

    return None


print("Tabla sodoku sin completar")
print("--------------------------")
print_board(tabla_sodoku)
resolver(tabla_sodoku)
print("--------------------------")
print("Tabla sodoku resuelta")
print("--------------------------")
print_board(tabla_sodoku)